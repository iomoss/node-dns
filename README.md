## node-dns

A simple RESTful server to do dns queries, replies with JSON.

## Usage:

Request to;
```
IP/dns?type=A4&payload=google.dk
```
Reply;
```
["109.105.109.238","109.105.109.218","109.105.109.251","109.105.109.227","109.105.109.219","109.105.109.234","109.105.109.212","109.105.109.208","109.105.109.229","109.105.109.241","109.105.109.230","109.105.109.245","109.105.109.223","109.105.109.216","109.105.109.240","109.105.109.249"]
```
