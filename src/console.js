var clc = require('cli-color');

var console = {}

// Curry output function
var outputter = function(prefix) {
    return function(msg)
    {
        process.stdout.write(prefix + ' ' + msg + '\n');
    };
}

console.log     = outputter(clc.cyan.bold('LOG:'));
console.info    = outputter(clc.green.bold('INFO:'));
console.warn    = outputter(clc.yellow.bold('WARN:'));
console.error   = outputter(clc.red.bold('ERROR:'));
console.assert  = outputter(clc.redBright.bold.inverse('ASSERT:'))

/*
console.log("Logging");
console.info("Infoing");
console.warn("Warning");
console.error("Erroring");
*/
module.exports = console;
