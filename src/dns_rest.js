var dns_rest = {};
// TODO: Add compression
// TODO: Reject requests with superfluous arguments
// TODO: Add error payloads
// TODO: Use === equals
// TODO: Add tests

var config = require('../config/config');

var express = require('express');
var cors = require('cors')
var app = express();
//app.use(cors);

var dns = require('dns');

var console = require('./console');
var gen_error = require('./errors').gen_error;


app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
 });

// Output information about the slicer-server
// Possibly serve a control page?
app.get('/', function(req, res, next) 
{
    res.writeHead(200, {'Content-Type': 'application/json'});
    // TODO: Clean up output
    res.end(JSON.stringify(process.env));
});

// Answer to dns requests
app.get('/dns', function(req, res, next)
{
    // TODO: Add this in
    //var type = req.query.type;
    var type = "A4";

    var payload = req.query.payload;
    if(!payload)
    {
        res.writeHead(400, {'Content-Type': 'application/json'});
	res.end(gen_error("2", "payload"));
    }
    else
    {
	// TODO: Switch and stuff
	dns.resolve4(payload, function(err, addresses)
	{
		if(err)
		{
			res.writeHead(500, {'Content-Type': 'application/json'});
			res.end(gen_error("3", err));
			return;
		}
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(addresses));
	});
    }
});

app.use(function(req, res, next)
{
    res.writeHead(404, {'Content-Type': 'application/json'});
    res.end(gen_error("1"));
});

// Start serving
dns_rest.start = function()
{
	// Bind to a port
	app.listen(config.port);
}

module.exports = dns_rest;
