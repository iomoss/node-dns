var console = require('./console');
var config = require('../config/config');

// TODO: Include suggested fix
var error_codes = {
   "0": {
        msg: "Not implemented!",
        severity: "6"
    },
    "1": {
        msg: "No such page",
        severity: "5"
    },
    "2": {
        msg: "Missing arugment for /dns (URL argument)",
        severity: "5"
    },
    "3": {
	msg: "Error resolving dns query",
	severity: "3"
    }
};

var error_severity = {
    "1": {
        msg: "ASSERT FAILED",
        output: console.assert
    },
    "2": {
        msg: "FATAL",
        output: console.error
    },
    "3": {
        msg: "CRITICAL",
        output: console.error
    },
    "4": {
        msg: "ERROR",
        output: console.error
    },
    "5": {
        msg: "WARN",
        output: console.warn
    },
    "6": {
        msg: "INFO",
        output: console.info
    },
    "7": {
        msg: "DEBUG",
        output: console.log
    }
}

var errors = {};
// Automatic error-generation
// `error_code` from 'error_codes',
// `payload` from wherever
// `severity_override` from 'error_severity'
// @Returns the error_string
// @Side-effect: error printing
errors.gen_error = function(error_code, payload, severity_override, disable_output)
{
    var error_json = error_codes[error_code];
    var severity = severity_override || error_json.severity;
    var severity_json = error_severity[severity];
    var json_error = {
        "error_code": error_code,
        "error_msg": error_json.msg,
        "error_payload": payload,
        "error_severity_code": severity,
        "error_severity": severity_json.msg
    };
    var error_string = JSON.stringify(json_error);
    if(severity < config.log_level && !disable_output)
    {
        severity_json.output(error_string);
    }
    return error_string;
}

module.exports = errors;
