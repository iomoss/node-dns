var cluster = require('cluster');

var console = require('./console');
var config = require('../config/config');

if (cluster.isMaster) 
{
	// Create a worker for each CPU
	for (var i = 0; i < config.workers; i += 1) 
	{
		cluster.fork();
	}

	cluster.on('exit', function(worker, code, signal) 
	{
		console.warn("Worker(" + worker.process.pid +') ' + worker.id + ' exited!');
		if(signal)
		{
			console.warn("--> Killed by signal: " + signal);
		}
		else if (code !== 0)
		{
			console.warn("--> Returned non-zero error code: " + code);
		}
		else
		{
			console.log("--> Ran succesfully");
		}
	});
}
else 
{
	var dns_server = require('./dns_rest');
	dns_server.start();
	console.log("Worker(" + cluster.worker.process.pid +') ' + cluster.worker.id + ' started!');
}
