#!/bin/bash

BOXES=$(which boxes)
if [ -z "$BOXES" ]; then
    BOXES=./forward.sh
fi

if [ $# -lt "1" ]; then
    echo -e "\n" \
            "Error: Missing an argument.\n" \
            "Usage: ./run.sh [WEBSERVER_PORT]\n" \
            "" | $BOXES -d stone 1>&2
    exit -1
fi

WEBSERVER_PORT=$1

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LOCAL_DIR=$(basename "$( dirname "$( dirname "$SCRIPT_DIR")")")
IMAGE_NAME="iomoss/$LOCAL_DIR"

echo -e "\n" \
        "Building image: '$IMAGE_NAME'\n" \
        "" | $BOXES -d stone |
echo -e "$(cat -)\n\n" \
        "Webserver mapped to: '$WEBSERVER_PORT'\n" \
        "" | $BOXES -d columns

echo "Image built succesfully; starting the build command!"
docker run -p $WEBSERVER_PORT:80 -it $IMAGE_NAME
