#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SRC_DIR="$(dirname "$(dirname $DIR)")"
PORT=$(cat config/config.js | grep "port:" | grep -o "[0-9][0-9]*")

CONTAINER_FOUND=$(docker ps -a | grep "slicer-server")
if [ -n "$CONTAINER_FOUND" ]; then
    echo "Old container found, removing...!"
    docker rm -f node-dns
fi

if [ $# -eq 0 ]; then
    echo "Running: npm run dev"
    docker run -it --rm --name node-dns -v "$SRC_DIR":/usr/src/app -w /usr/src/app -p $PORT:$PORT node:4.2.2-slim npm run dev
else
    echo "Running: $@"
    docker run -it --rm --name node-dns -v "$SRC_DIR":/usr/src/app -w /usr/src/app -p $PORT:$PORT node:4.2.2-slim $@
fi
