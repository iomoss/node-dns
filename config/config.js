var config = {

//--------------------------------------------------------------------
// Webinterface configuration
// The port which the interface is hosted at
    port: 8011,


//--------------------------------------------------------------------
// Cluster configuration
// The number of duplicate processes
    workers: require('os').cpus().length,
    // workers: 8,


//--------------------------------------------------------------------
// Logging configuration
// The lowest error-severity to see
    log_level: 7

}

module.exports = config;
